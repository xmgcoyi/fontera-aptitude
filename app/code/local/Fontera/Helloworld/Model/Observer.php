<?php
class Fontera_Helloworld_Model_Observer extends Varien_Event_Observer
{
	public function __construct() {
	}

	 public function changeCustomerName(Varien_Event_Observer $observer) {
	 	$event = $observer->getEvent();
		$customer = $event->getCustomer();
		$customer->setFirstname('Developer');
		$customer->save();
		return $this;
	}
}