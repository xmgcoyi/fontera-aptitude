<?php
class Fontera_Helloworld_IndexController extends Mage_Core_Controller_Front_Action {
    public function indexAction() {
        echo 'Hello World';
        $this->loadLayout();
        $this->renderLayout();
    }

   public function goodbyeAction() {
	    echo 'Sorry to see you go';
	    $this->loadLayout();
        $this->renderLayout();
	}
}
